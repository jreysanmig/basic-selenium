package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class HomePage extends BasePage{

	public HomePage(WebDriver driver) {
		super(driver);
	}
	
	public HomePage enterSearchText(String searchTxt) {
		this.driver.findElement(By.id("searchterm")).sendKeys(searchTxt);
		return this;
	}
	
	public SearchPage submitSearch() {
		this.driver.findElement(By.id("searchterm")).sendKeys(Keys.ENTER);
		return new SearchPage(driver);
	}

}
