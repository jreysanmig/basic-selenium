package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class BasePage {
	protected WebDriver driver;
	
	public BasePage(WebDriver driver) {
		this.driver = driver;
	}
	
	public StoreLocatorPage clickStoreLocator() {
		this.driver.findElement(By.cssSelector("li.contact-us a")).click();
		return new StoreLocatorPage(driver);
	}
	
	public CustomerFeedbackPage clickContactUs() {
		this.driver.findElement(By.cssSelector("li.contact-us a")).click();
		return new CustomerFeedbackPage(driver);
	}
}
