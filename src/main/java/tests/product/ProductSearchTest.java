package tests.product;

import org.junit.Test;

import pages.HomePage;
import pages.SearchPage;
import tests.BaseTest;

public class ProductSearchTest extends BaseTest {
	HomePage homePage;
	SearchPage searchPage;
	
	@Test
	public void productSearchTest() {
		homePage = new HomePage(driver);
		
		searchPage = homePage
						.enterSearchText("lacoste")
						.submitSearch();
	}

}
